<?php

namespace Drupal\Tests\braintree_donations\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests basic module functionality.
 *
 * @group Braintree Donations
 */
class BasicTest extends KernelTestBase  {

  public static $modules = ['braintree_donations', 'key'];

  /**
   * Tests module install and configuration.
   */
  function testConfiguraiton() {
    $this->assertTrue(TRUE);
  }
}