<?php

namespace Drupal\braintree_donations;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\braintree_donations\Utility\AES_128_CBC;
use Drupal\key\KeyRepositoryInterface;

class BraintreeGatewayFactory implements BraintreeGatewayFactoryInterface {

  /**
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * @var array
   */
  protected $settings;

  /**
   * @var string
   */
  protected $environment;

  /**
   * Creates a BraintreeGatewayFactory to return \Braintree\Gateway instances.
   *
   * @param $site_settings \Drupal\Core\Site\Settings
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   */
  public function __construct(Settings $site_settings, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository) {
    $this->environment = $site_settings->get('braintree_donations.environment', 'sandbox');
    $this->settings = (array) $config_factory->get('braintree_donations.api_settings')->get($this->environment);
    $this->settings['environment'] = $this->environment;
    $this->keyRepository = $key_repository;
  }

  /**
   * Gets the a gateway if Braintree is configured.
   *
   * @return \Braintree\Gateway|NULL
   *
   * @throws \Braintree\Exception\Configuration
   *   If the api settings are invalid.
   */
  public function get() {
    $gateway = NULL;
    if (class_exists('\Braintree\Gateway')) {
      $settings = $this->settings;
      $key_id = $settings['encryption_key'];
      $key_object = $this->keyRepository->getKey($key_id);
      if ($key_object && ($key = $key_object->getKeyValue())) {
        $settings['privateKey'] = AES_128_CBC::decrypt($settings['privateKey'], $key);
        if ($settings['merchantId'] && $settings['privateKey']) {
          $gateway = new \Braintree\Gateway($settings);
        }
      }
    }
    return $gateway;
  }
}