<?php

namespace Drupal\braintree_donations;

interface GatewayInterface {

  /**
   * Return a list of country codes and readable names.
   *
   * @return array
   */
  public function countries();

  /**
   * Gets a Braintree gateway if Braintree is configured.
   *
   * @return \Braintree\Gateway|NULL
   */
  public function get();

  public function sale(array $transaction, $recurring_plan = '');

  public function cleanTransaction(array $transaction);

}