<?php

namespace Drupal\braintree_donations;

interface BraintreeGatewayFactoryInterface {

  /**
   * Gets a Braintree gateway if Braintree is configured.
   *
   * @return \Braintree\Gateway|NULL
   */
  public function get();

}