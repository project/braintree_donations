<?php

namespace Drupal\braintree_donations\Controller;

use Drupal\braintree_donations\Form\RecurringRequeueForm;
use Drupal\braintree_donations\Form\RecurringSettingsForm;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RecurringPage extends ControllerBase {

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue')
    );
  }

  /**
   * RecurringPage constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $factory
   */
  public function __construct(QueueFactory $factory) {
    $this->queueFactory = $factory;
  }

  public function adminForms() {
    $build = [];
    $failed_queue = $this->queueFactory->get('braintree_donations_recurring_failed', TRUE);
    $failed_queue->createQueue();
    $count = $failed_queue->numberOfItems();
    if ($count > 0) {
      $build[] = $this->formBuilder()->getForm(RecurringRequeueForm::class);
      // @todo use format_plural() for better grammar.
      drupal_set_message($this->t('There are @count transactions in the %fail_queue queue that failed in creating a recurring charge.', [
        '@count' => $count,
        '%fail_queue' => 'braintree_donations_recurring_failed',
      ]), 'error');
    }
    $build[] = $this->formBuilder()->getForm(RecurringSettingsForm::class);
    return $build;
  }
}