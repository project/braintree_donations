<?php /**
 * @file
 * Contains \Drupal\braintree_donations\Controller\TokenController.
 */

namespace Drupal\braintree_donations\Controller;

use Drupal\braintree_donations\BraintreeGatewayFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Default controller for the braintree_donations module.
 */
class TokenController extends ControllerBase {

  /**
   * @var \Drupal\braintree_donations\BraintreeGatewayFactoryInterface
   */
  protected $braintreeGatewayFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('braintree_donations.braintree_gateway_factory')
    );
  }

  /**
   * TokenController constructor.
   *
   * @param \Drupal\braintree_donations\BraintreeGatewayFactoryInterface $factory
   */
  public function __construct(BraintreeGatewayFactoryInterface $factory) {
    $this->braintreeGatewayFactory = $factory;
  }

  public function clientToken() {
    $token = NULL;
    try {
      /** @var \Braintree\Gateway $gateway */
      $gateway = $this->braintreeGatewayFactory->get();
      if ($gateway) {
        $token = $gateway->clientToken()->generate();
      }
    }
    catch (\Exception $e) {
      watchdog_exception('braintree', $e);
    }
    return new JsonResponse(['token' => $token]);
  }

  public function adminPage() {
    $build = [];
    $failed_queue = \Drupal::queue('braintree_donations_recurring_failed', TRUE);
    $failed_queue->createQueue();
    $count = $failed_queue->numberOfItems();
    if ($count > 0) {
      $build[] = $this->formBuilder()->getForm('braintree_donations_recurring_requeue_form');
      // @todo use format_plural() for better grammar.
      drupal_set_message(t('There are @count transactions in the %fail_queue queue that failed in creating a recurring charge.', [
        '@count' => $count,
        '%fail_queue' => 'braintree_donations_recurring_failed',
      ]), 'error');
    }
    $build[] = $this->formBuilder()->getForm('braintree_donations_recurring_settings_form');
    return $build;
  }

}
