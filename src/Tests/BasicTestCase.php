<?php
namespace Drupal\braintree_donations\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests basic functionality of the Braintree Donations module.
 *
 * @group Braintree Donations
 */
class BasicTestCase extends WebTestBase {

  /**
   * A user with permission to administer site configuration.
   *
   * @var object
   */
  protected $adminUser;


  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('key', 'braintree_donations');

  protected function setUp() {
    parent::setUp();
  }


  /**
   * Tests donation functionality.
   */
  function testDonation() {
    $this->drupalGet('braintree-dropin-donations');
    $this->assertResponse('200', 'Default donation page found.');
    // Since the gateway is ot configured, the form should be blank.
    $this->assertText(t('You must configure your api keys and run composer install before you can connect to the payments system.'));

  }

}
