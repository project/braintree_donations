<?php

namespace Drupal\braintree_donations;


use Braintree\TransactionGateway;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\Core\Queue\QueueFactory;

class Gateway implements GatewayInterface {

  /**
   * @var \Drupal\braintree_donations\BraintreeGatewayFactoryInterface
   */
  protected $braintreeGatewayFactory;

  /**
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;


  /**
   * Gateway constructor.
   *
   * @param \Drupal\braintree_donations\BraintreeGatewayFactoryInterface $gateway_factory
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   */
  public function __construct(BraintreeGatewayFactoryInterface $gateway_factory, CountryManagerInterface $country_manager, QueueFactory $queue_factory) {
    $this->braintreeGatewayFactory = $gateway_factory;
    $this->countryManager = $country_manager;
    $this->queueFactory = $queue_factory;
  }

  public function countries() {
    return array_intersect_key($this->countryManager->getList(), array_flip($this->braintreeAllowedCountries()));
  }

  public function get() {
    return $this->braintreeGatewayFactory->get();
  }

  /**
   * Truncate string to 255 characters.
   *
   * @param string $string
   *
   * @return string
   */
  protected function truncate($string) {
    return Unicode::truncate($string, 255);
  }

  public static function validTransactionKeys() {
    $keys = [];
    // @see https://developers.braintreepayments.com/reference/request/transaction/sale/php#amount
    $signature = TransactionGateway::createSignature();
    foreach ($signature as $field) {
      if (is_array($field)) {
        $field = current(array_keys($field));
      }
      $keys[$field] = TRUE;
    }
    return $keys;
  }

  public function cleanTransaction(array $transaction) {
    $clean_transaction = [];
    if (!isset($transaction['billing']['firstName'])) {
      $transaction['billing']['firstName'] = $transaction['customer']['firstName'];
    }
    if (!isset($transaction['billing']['lastName'])) {
      $transaction['billing']['lastName'] = $transaction['customer']['lastName'];
    }
    $transaction['customFields'] = empty($transaction['customFields']) ? [] : array_map($transaction['customFields'], [$this, 'truncate']);
    $transaction["options"] = [
      "submitForSettlement" => TRUE,
    ];
    $transaction['recurring'] = FALSE;
    $valid_keys = static::validTransactionKeys();
    foreach ($transaction as $key => $value) {
      if (!empty($valid_keys[$key])) {
        $clean_transaction[$key] = $value;
      }
    }
    return $clean_transaction;
  }

  /**
   * @param array $transaction
   * @param string $recurring_plan
   *
   * @return \Braintree\Result\Successful|\Braintree\Result\Error
   *
   * @throws \Exception
   */
  public function sale(array $transaction, $recurring_plan = '') {
    $transaction = $this->cleanTransaction($transaction);
    $recurring_plan = trim($recurring_plan);
    if ($recurring_plan) {
      // For now, each customer and and card token are the same random value. Use
      // base 36, since Braintree seems to consider IDs to be case insensitive.
      // The minimum value for mt_rand() is needed to insure the base 36 prefix
      // has a constant string length.
      $prefix = base_convert(mt_rand(60466176, mt_getrandmax()), 10, 36);
      $token = str_replace('.', '', uniqid($prefix, TRUE));
      $transaction['customer']['id'] = $token;
      $transaction['creditCard']['token'] = $token;
      $transaction['options']['storeInVaultOnSuccess'] = TRUE;
      $transaction['options']['addBillingAddressToPaymentMethod'] = TRUE;
      $transaction['recurring'] = TRUE;
    }
    // Throws Exception on failure.
    $result = $this->get()->transaction()->sale($transaction);
    if (!empty($result->success) && $recurring_plan) {
      // Grab the plan ID.
      $transaction['recurring_plan'] = $recurring_plan;
      // Store the data in a queue for later setup of recurring payment plan.
      $queue = \Drupal::queue('braintree_donations_recurring', TRUE);
      $queue->createQueue();
      $queue->createItem($transaction);
    }
    return $result;
  }

  protected function braintreeAllowedCountries() {
    // List of supported 2-letter country codes from
    // https://developers.braintreepayments.com/reference/general/countries/php#list-of-countries
    return [
      'AF',
      'AX',
      'AL',
      'DZ',
      'AS',
      'AD',
      'AO',
      'AI',
      'AQ',
      'AG',
      'AR',
      'AM',
      'AW',
      'AU',
      'AT',
      'AZ',
      'BS',
      'BH',
      'BD',
      'BB',
      'BY',
      'BE',
      'BZ',
      'BJ',
      'BM',
      'BT',
      'BO',
      'BQ',
      'BA',
      'BW',
      'BV',
      'BR',
      'IO',
      'BN',
      'BG',
      'BF',
      'BI',
      'KH',
      'CM',
      'CA',
      'CV',
      'KY',
      'CF',
      'TD',
      'CL',
      'CN',
      'CX',
      'CC',
      'CO',
      'KM',
      'CG',
      'CD',
      'CK',
      'CR',
      'CI',
      'HR',
      'CU',
      'CW',
      'CY',
      'CZ',
      'DK',
      'DJ',
      'DM',
      'DO',
      'EC',
      'EG',
      'SV',
      'GQ',
      'ER',
      'EE',
      'ET',
      'FK',
      'FO',
      'FJ',
      'FI',
      'FR',
      'GF',
      'PF',
      'TF',
      'GA',
      'GM',
      'GE',
      'DE',
      'GH',
      'GI',
      'GR',
      'GL',
      'GD',
      'GP',
      'GU',
      'GT',
      'GG',
      'GN',
      'GW',
      'GY',
      'HT',
      'HM',
      'HN',
      'HK',
      'HU',
      'IS',
      'IN',
      'ID',
      'IR',
      'IQ',
      'IE',
      'IM',
      'IL',
      'IT',
      'JM',
      'JP',
      'JE',
      'JO',
      'KZ',
      'KE',
      'KI',
      'KP',
      'KR',
      'KW',
      'KG',
      'LA',
      'LV',
      'LB',
      'LS',
      'LR',
      'LY',
      'LI',
      'LT',
      'LU',
      'MO',
      'MK',
      'MG',
      'MW',
      'MY',
      'MV',
      'ML',
      'MT',
      'MH',
      'MQ',
      'MR',
      'MU',
      'YT',
      'MX',
      'FM',
      'MD',
      'MC',
      'MN',
      'ME',
      'MS',
      'MA',
      'MZ',
      'MM',
      'NA',
      'NR',
      'NP',
      'NL',
      'AN',
      'NC',
      'NZ',
      'NI',
      'NE',
      'NG',
      'NU',
      'NF',
      'MP',
      'NO',
      'OM',
      'PK',
      'PW',
      'PS',
      'PA',
      'PG',
      'PY',
      'PE',
      'PH',
      'PN',
      'PL',
      'PT',
      'PR',
      'QA',
      'RE',
      'RO',
      'RU',
      'RW',
      'BL',
      'SH',
      'KN',
      'LC',
      'MF',
      'PM',
      'VC',
      'WS',
      'SM',
      'ST',
      'SA',
      'SN',
      'RS',
      'SC',
      'SL',
      'SG',
      'SX',
      'SK',
      'SI',
      'SB',
      'SO',
      'ZA',
      'GS',
      'SS',
      'ES',
      'LK',
      'SD',
      'SR',
      'SJ',
      'SZ',
      'SE',
      'CH',
      'SY',
      'TW',
      'TJ',
      'TZ',
      'TH',
      'TL',
      'TG',
      'TK',
      'TO',
      'TT',
      'TN',
      'TR',
      'TM',
      'TC',
      'TV',
      'UG',
      'UA',
      'AE',
      'GB',
      'UM',
      'US',
      'UY',
      'UZ',
      'VU',
      'VA',
      'VE',
      'VN',
      'VG',
      'VI',
      'WF',
      'EH',
      'YE',
      'ZM',
      'ZW',
    ];
  }

}