<?php

/**
 * @file
 * Contains \Drupal\braintree_donations\Form\RecurringSettingsForm.
 */

namespace Drupal\braintree_donations\Form;

use Drupal\braintree_donations\GatewayInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RecurringSettingsForm extends ConfigFormBase  {
  use GatewayFactoryTrait;

  /**
   * RecurringSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\braintree_donations\GatewayInterface $gateway
   */
  public function __construct(ConfigFactoryInterface $config_factory, GatewayInterface $gateway) {
    parent::__construct($config_factory);
    $this->gateway = $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('braintree_donations.gateway')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['braintree_donations.recurring_settings'];
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'braintree_donations_api_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->missingApiSettings($form)) {
      return $form;
    }
    $plans = array();
    try {
      // Re-key plan data by the ID.
      /** @var \Braintree\Gateway $braintree_gateway */
      $braintree_gateway = $this->getBraintreeGateway();
      foreach ($braintree_gateway->plan()->all() as $plan) {
        $plans[$plan->id] = $plan;
      }
    }
    catch (\Braintree\Exception $e) {
      braintree_donations_handle_exception($e);
      return $form;
    }

    $defaults = $this->config('braintree_donations.recurring_settings')->get();

    $form['#plan_data'] = $plans;
    $form['plans'] = array(
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Available recurring donation plans')
    );
    foreach ($plans as $plan) {
      $form['plans'][$plan->id] = array(
        '#title' => Html::escape($plan->name),
        '#type' => 'checkbox',
        '#description' => t('%description plan with a billing cycle of every @count months', array('%description' => $plan->description, '@count' => $plan->billingFrequency)),
        '#default_value' => !empty($defaults['plans'][$plan->id]),
      );
    }
    $form['queue_on_cron'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable processing of recurring on Drupal cron. Un-check if you are processing using the drush command.'),
      '#default_value' => $defaults['queue_on_cron'],
    );

    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    );
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plans = array_filter($form_state->getValue('plans'));
    $frequencies = array();
    // Save the plan names for use in donation forms.
    foreach($plans as $id => $v) {
      $plans[$id] = $form['#plan_data'][$id]->name;
      $frequencies[$id] = $form['#plan_data'][$id]->billingFrequency;
    }
    $config = $this->config('braintree_donations.recurring_settings');
    $config->set('plans', $plans);
    $config->set('frequencies', $frequencies);
    $config->set('queue_on_cron', $form_state->getValue('queue_on_cron'));
    $config->save();
    drupal_set_message(t('The configuration options have been saved.'));
  }

}
