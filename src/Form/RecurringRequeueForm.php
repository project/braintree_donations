<?php

namespace Drupal\braintree_donations\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RecurringRequeueForm extends FormBase {

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue')
    );
  }

  /**
   * RecurringPage constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $factory
   */
  public function __construct(QueueFactory $factory) {
    $this->queueFactory = $factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'braintree_donations_recurring_requeue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Re-try failed recurring transactions'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $failed_queue = $this->queueFactory->get('braintree_donations_recurring_failed', TRUE);
    $queue =  $this->queueFactory->get('braintree_donations_recurring', TRUE);
    $count = 0;
    while ($item = $failed_queue->claimItem()) {
      $transaction = $item->data;
      $transaction['retry_at'] = 0;
      $queue->createItem($transaction);
      $failed_queue->deleteItem($item);
      $count++;
    }
    // @todo use format_plural().
    drupal_set_message($this->t('@count previously failed items were moved back from the %fail_queue queue.', array('@count' => $count, '%fail_queue' => 'braintree_donations_recurring_failed')));
  }

}
