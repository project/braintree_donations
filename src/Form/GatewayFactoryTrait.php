<?php

namespace Drupal\braintree_donations\Form;


trait GatewayFactoryTrait {

  /**
   * @var \Drupal\braintree_donations\GatewayInterface
   */
  protected $gateway;

  /**
   * Helper function to populate a warning message on forms.
   */
  protected function missingApiSettings(&$form) {
    /** @var \Braintree\Gateway $gateway */
    $braintree_gateway = $this->getBraintreeGateway();
    if (empty($braintree_gateway)) {
      $form['problem'] = array(
        '#prefix' => '<div class="messages warning">',
        '#markup' => t('You must configure your api keys and run composer install before you can connect to the payments system.'),
        '#suffix' => '</div>',
      );
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return \Braintree\Gateway|NULL
   */
  protected function getBraintreeGateway() {
    return $this->gateway->get();
  }

}