<?php

/**
 * @file
 * Contains \Drupal\braintree_donations\Form\CustomerSettingsForm.
 */

namespace Drupal\braintree_donations\Form;

use Drupal\braintree_donations\GatewayInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomerSettingsForm extends ConfigFormBase {
  use GatewayFactoryTrait;

  /**
   * CustomerSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\braintree_donations\GatewayInterface $factory
   */
  public function __construct(ConfigFactoryInterface $config_factory, GatewayInterface $gateway) {
    parent::__construct($config_factory);
    $this->gateway = $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('braintree_donations.gateway')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['braintree_donations.customer_defaults', 'braintree_donations.form_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'braintree_donations_customer_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->missingApiSettings($form)) {
      return $form;
    }
    $defaults = $this->config('braintree_donations.customer_defaults')->get();
    $form['defaults'] = ['#tree' => TRUE];
    $form['defaults']['locality'] = [
      '#type' => 'textfield',
      '#title' => t('Default city'),
      '#default_value' => $defaults['locality'],
    ];
    $form['defaults']['region'] = [
      '#type' => 'textfield',
      '#title' => t('Default state'),
      '#default_value' => $defaults['region'],
      '#maxlength' => 4,
      '#size' => 4,
    ];
    $form['defaults']['postal_code'] = [
      '#type' => 'textfield',
      '#title' => t('Default ZIP code'),
      '#size' => 5,
      '#default_value' => $defaults['postal_code'],
    ];
    $form['defaults']['country_code_alpha2'] = [
      '#type' => 'textfield',
      '#title' => t('Default 2 Letter country code for all transactions'),
      '#description' => 'See https://developers.braintreepayments.com/javascript+php/reference/general/countries',
      '#default_value' => $defaults['country_code_alpha2'],
      '#required' => TRUE,
      '#maxlength' => 2,
      '#size' => 2,
    ];
    $form['defaults']['country_code_user_choose'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow the user to choose a country (otherwise the default is always used)'),
      '#default_value' => $defaults['country_code_user_choose'],
    ];
    $form['defaults']['country_code_user_choose'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow the user to choose a country (otherwise the default is always used)'),
      '#default_value' => $defaults['country_code_user_choose'],
    ];
    $form['defaults']['country_codes_alpha2_priorities'] = [
      '#type' => 'textfield',
      '#description' => t('Comma-separated list of 2 letter county codes that should be moved to the front of the list.'),
      '#title' => t('List of priority county codes'),
      '#default_value' => implode(', ', $defaults['country_codes_alpha2_priorities']),
    ];
    $form['defaults']['country_codes'] = [
      '#type' => 'details',
      '#title' => t('List of county codes'),
      '#open' => FALSE,
    ];
    $rows = [];
    foreach (braintree_donations_countries() as $code => $name) {
      $rows[] = [$code, $name];
    }
    $form['defaults']['country_codes']['table'] = [
      '#theme' => 'table',
      '#header' => [
        t('2 Letter Code'),
        t('County Name'),
      ],
      '#rows' => $rows,
    ];
    $pages = $this->config('braintree_donations.form_settings')->get();
    $form['pages']['require_billing_address'] = [
      '#type' => 'checkbox',
      '#title' => t('Require billing address in addition to postal code'),
      '#default_value' => $pages['require_billing_address'],
    ];

    // url() expects a route name or an external URI.
    $form['pages']['enable_dropin_form'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable dropin form at /braintree-dropin-donations'),
      '#default_value' => $pages['enable_dropin_form'],
  );

    $form['pages']['js_redirect'] = [
      '#type' => 'textfield',
      '#title' => t('Donation Form Redirect'),
      '#description' => t('Path where form submissions should be redirected on success.'),
      '#default_value' => $pages['js_redirect'],
    ];
    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save defaults'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $countries = $this->gateway->countries();
    $code = $form_state->getValue(['defaults', 'country_code_alpha2']);
    if (empty($countries[$code])) {
      $form_state->setErrorByName("defaults][country_code_alpha2", t('Invalid county code %code.', [
        '%code' => $code
        ]));
    }
    $priorities = explode(',', $form_state->getValue(['defaults', 'country_codes_alpha2_priorities']));
    $form_state->setValue(['defaults', 'country_codes_alpha2_priorities'], []);
    foreach ($priorities as $code) {
      $code = trim($code);
      if (empty($countries[$code])) {
        $form_state->setErrorByName("defaults][country_codes_alpha2_priorities", t('Invalid county code %code.', [
          '%code' => $code
          ]));
      }
      else {
        // Transform this into an array of codes.
        $form_state->setValue(['defaults', 'country_codes_alpha2_priorities', $code], $code);
      }
    }
    $js_redirect = $form_state->getValue('js_redirect');
    try {
       $url = Url::fromUserInput($js_redirect);
       $url->toString();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('js_redirect', $this->t('Invalid redirect path. @message', ['@message' => $e->getMessage()]));
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $defauts_config  = $this->config('braintree_donations.customer_defaults');
    $defauts_config->setData($form_state->getValue('defaults'));
    $defauts_config->save();
    $form_config = $this->config('braintree_donations.form_settings');
    foreach (['require_billing_address', 'enable_dropin_form', 'js_redirect'] as $key) {
      $form_config->set($key, $form_state->getValue($key));
    }
    $form_config->save();
    drupal_set_message(t('The configuration options have been saved.'));
  }

}
