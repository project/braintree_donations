<?php

/**
 * @file
 * Contains \Drupal\braintree_donations\Form\BraintreeDonationsDropinForm.
 */

namespace Drupal\braintree_donations\Form;

use Drupal\braintree_donations\GatewayInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DropinForm extends FormBase {
  use GatewayFactoryTrait;

  /**
   * @var string
   */
  protected $environment;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $apiSettings;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $customerDefaults;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $formSettings;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $recurringSettings;

  /**
   * DropinForm constructor.
   *
   * @param $environment
   * @param \Drupal\Core\Config\Config $api_settings
   * @param \Drupal\Core\Config\Config $customer_defaults
   * @param \Drupal\Core\Config\Config $form_settings
   * @param \Drupal\Core\Config\Config $recurring_settings
   * @param \Drupal\braintree_donations\GatewayInterface $gateway
   */
  public function __construct($environment, Config $api_settings, Config $customer_defaults, Config $form_settings, Config $recurring_settings, GatewayInterface $gateway) {
    $this->environment = $environment;
    $this->apiSettings = $api_settings;
    $this->customerDefaults = $customer_defaults;
    $this->formSettings = $form_settings;
    $this->recurringSettings = $recurring_settings;
    $this->gateway = $gateway;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $config_factory = $container->get('config.factory');
    $environment = $container->get('settings')->get('braintree_donations.environment', 'sandbox');
    return new static(
      $environment,
      $config_factory->get('braintree_donations.api_settings'),
      $config_factory->get('braintree_donations.customer_defaults'),
      $config_factory->get('braintree_donations.form_settings'),
      $config_factory->get('braintree_donations.recurring_settings'),
      $container->get('braintree_donations.gateway')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'braintree_donations_dropin_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->missingApiSettings($form)) {
      return $form;
    }

    // Browsers and proxies should never store the credit card form.
    $form['#attached']['http_header'] = [
      ['Cache-Control', 'no-store, must-revalidate, no-cache, private', TRUE],
    ];

    $advanced_fraud_detect = $this->apiSettings->get($this->environment)['advanced_fraud_detection'] > 0 ? TRUE : FALSE;
    $settings = [
      'environment' => $this->environment,
      'advancedFraudDetect' => $advanced_fraud_detect,
    ];

    $form['#attached']['drupalSettings']['BraintreeDonations'] = $settings;
    $form['#attached']['library'][] = 'braintree_donations/dropin-form';

    // Build the credit card form.
    $form['no-js-warning'] = [
      '#type' => 'item',
      '#id' => 'braintree-no-js-warning',
      '#markup' => '<div class="messages warning">' . $this->t('JavaScript must be enabled to use this donation form') . '</div>',
      '#attributes' => [
        'class' => [
          'warning'
          ]
        ],
    ];

    $form['braintree_donations']['transaction'] = ['#tree' => TRUE];

    $form['braintree_donations']['transaction']['amount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amount'),
      '#field_prefix' => '$',
      '#default_value' => '100',
      '#required' => TRUE,
      '#maxlength' => 8,
      '#size' => 6,
    ];
    // Add recurring options which we can support with server-to-server calls.
    $recurring_plans = (array) $this->recurringSettings->get('plans');
    $options = [' ' => $this->t('Once')] + $recurring_plans;
    $form['braintree_donations']['transaction']['recurring_plan'] = [
      '#access' => count($options) > 1,
      '#title' => $this->t('Make this donation'),
      '#type' => 'radios',
      '#default_value' => ' ',
      '#options' => $options,
    ];
    $form['braintree_donations']['transaction']['dropin'] = ['#type' => 'item'];

    $this->customerFormSection($form);
    $form['braintree_donations']['payment_method_nonce'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];
    $form['braintree_donations']['transaction']['deviceData'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];

    $form['action']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Donate'),
      '#prefix' => '<div class="braintree-donations-credit-card-submit">',
      '#suffix' => '</div>',
      '#weight' => 100,
    ];

    return $form;
  }

  public function customerFormSection(&$form) {
    $form['braintree_donations']['transaction']['customer']['firstName'] = array(
      '#type' => 'textfield',
      '#prefix' => '<div class="braintree-donations-input-inline">',
      '#title' => $this->t('First name'),
      '#default_value' => '',
      '#required' => TRUE,
      '#attributes' => array('required' => NULL),
      '#size' => 20,
      '#maxlength' => 40,
    );
    $form['braintree_donations']['transaction']['customer']['lastName'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#default_value' => '',
      '#required' => TRUE,
      '#attributes' => array('required' => NULL),
      '#size' => 25,
      '#maxlength' => 40,
      '#suffix' => '<br class="clear-float" /></div>',
    );
    $form['braintree_donations']['transaction']['customer']['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#default_value' => '',
      '#required' => TRUE,
      '#attributes' => array(
        'title' => $this->t('A valid email address')
      ),
      '#size' => 52,
      '#maxlength' => 255,
    );
    $require_address = $this->formSettings->get('require_billing_address');
    $address_attributes = array();
    if ($require_address) {
      $address_attributes['required'] = NULL;
    }
    $form['braintree_donations']['transaction']['billing']['streetAddress'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Billing street address'),
      '#default_value' => '',
      '#required' => $require_address,
      '#attributes' => $address_attributes,
      '#size' => 52,
      '#maxlength' => 255,
    );

    $form['braintree_donations']['transaction']['billing']['locality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => $this->customerDefaults->get('locality'),
      '#required' => $require_address,
      '#attributes' => $address_attributes,
      '#size' => 20,
      '#maxlength' => 255,
      '#prefix' => '<div class="braintree-donations-input-inline">',
    );
    // UK counties can be up to 24 characters.
    $form['braintree_donations']['transaction']['billing']['region'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('State or Region'),
      '#default_value' => $this->customerDefaults->get('region'),
      '#required' => $require_address,
      '#attributes' => $address_attributes,
      '#maxlength' => 24,
      '#size' => 6,
    );
    $form['braintree_donations']['transaction']['billing']['postalCode'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Zip / Postal code'),
      '#default_value' => $this->customerDefaults->get('postal_code'),
      '#required' => TRUE,
      '#attributes' => array('required' => NULL),
      '#size' => 10,
      '#maxlength' => 10,
      '#suffix' => '<br class="clear-float" /></div>',
    );
    if ($this->customerDefaults->get('country_code_user_choose')) {
      $options = $this->gateway->countries();
      $priority_countries = $this->customerDefaults->get('country_codes_alpha2_priorities');
      if (!empty($priority_countries)) {
        $new_options = array();
        foreach ($priority_countries as $code) {
          $new_options[$code] = $options[$code];
        }
        // Use an empty optgroup as separator.
        $new_options['-----------'] = array();
        $new_options += array_diff_key($options, $priority_countries);
        $options = $new_options;
      }

      $form['braintree_donations']['transaction']['billing']['countryCodeAlpha2'] = array(
        '#type' => 'select',
        '#title' => $this->t('Country'),
        '#options' => $options,
        '#default_value' => $this->customerDefaults->get('country_code_alpha2'),
      );
    }
    else {
      $form['braintree_donations']['transaction']['billing']['countryCodeAlpha2'] = array(
        '#type' => 'value',
        '#value' => $this->customerDefaults->get('country_code_alpha2'),
      );
    }
    $form['braintree_donations']['transaction']['customFields'] = array(
      // Add any custom fields configured in your Braintree account to this section.
    );

    $form['#attached']['library'][] = 'braintree_donations/card-form-layout';

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle single-step and multi-step submissions.
    if ($stored = $form_state->get('values')) {
      // Values were stored in a prior multi-step.
      $values = $stored;
    }
    else {
      $values = $form_state->getValues();
    }
    $transaction = $values['transaction'];
    $transaction['paymentMethodNonce'] = $values['payment_method_nonce'];
    try {
      $result = $this->gateway->sale($transaction, $transaction['recurring_plan']);
    }
    catch (\Exception $e) {
      $result = new \stdClass();
      $result->message = get_class($e) . "\n" . $e->getMessage();
    }
    // Allow other modules access to the stored values.
    $form_state->set('submitted-values', $values);
    if (!empty($result->success)) {
      drupal_set_message("Success: your payment was submitted with ID " . $result->transaction->id);
      $form_state->setRedirectUrl(Url::fromUserInput($this->formSettings->get('js_redirect')));
    }
    else {
      if (isset($result->message)) {
        $error = Markup::create(nl2br(\Drupal\Component\Utility\Html::escape($result->message)));
      }
      else {
        $error = Markup::create(nl2br(\Drupal\Component\Utility\Html::escape(print_r($result, TRUE))));
      }
      if (!empty($result->transaction)) {
        $message = $this->t("Error creating transaction:<br/>@error_text", ['@error_text' => $error]);
      }
      else {
        $message = $this->t('Processing failed:<br/>@error_text', ['@error_text' => $error]);
      }
      drupal_set_message($message, 'error');
      $form_state->setRebuild();
      $this->logger('braintree_donations')->error($message, []);
    }
  }

}
