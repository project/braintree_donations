<?php

/**
 * @file
 * Contains \Drupal\braintree_donations\Form\ApiSettingsForm.
 */

namespace Drupal\braintree_donations\Form;

use Drupal\braintree_donations\Utility\AES_128_CBC;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiSettingsForm extends ConfigFormBase {

  /**
   * @var string
   */
  protected $environment;

  /**
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Creates a ApiSettingsForm.
   *
   * @param string $environment
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   Repository to get the encryption key.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   CSS and JS library manager.
   *
   */
  public function __construct($environment, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, LibraryDiscoveryInterface $library_discovery) {
    parent::__construct($config_factory);
    $this->environment = $environment;
    $this->keyRepository = $key_repository;
    $this->libraryDiscovery = $library_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $environment = $container->get('settings')->get('braintree_donations.environment', 'sandbox');
    return new static(
      $environment,
      $container->get('config.factory'),
      $container->get('key.repository'),
      $container->get('library.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'braintree_donations_api_settings_form';
  }

  protected function getEditableConfigNames() {
    return ['braintree_donations.api_settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $options = [
      'sandbox' => $this->t('Sandbox - used for testing, requires a Braintree Sandbox account'),
      'production' => $this->t('Production - used for processing real transactions'),
    ];
    $form['environment'] = [
      '#type' => 'item',
      '#title' => $this->t('Current Braintree server environment:'),

      '#markup' => '<p><em>' . $this->environment . '</em></p>',
    ];

    $form['settings'] = ['#tree' => TRUE];
    foreach ($options as $environment => $description) {
      $form['settings'][$environment] = [
        '#type' => 'details',
        '#title' => $description,
        '#open' => ($environment == $this->environment),
      ];
      $settings = (array) $this->config('braintree_donations.api_settings')->get($environment);
      $form['settings'][$environment]['encryption_key'] = array(
        '#type' => 'key_select',
        '#title' => $this->t('Encryption key'),
        '#key_filters' => ['type_group' => 'encryption'],
        '#default_value' => $settings['encryption_key'],
        '#required' => ($environment == $this->environment),
      );
      $form['settings'][$environment]['merchantId'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Merchant ID'),
        '#default_value' => $settings['merchantId'],
        '#required' => ($environment == $this->environment),
      ];
      $form['settings'][$environment]['publicKey'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Public key'),
        '#default_value' => $settings['publicKey'],
        '#required' => ($environment == $this->environment),
      ];
      $display = !empty($settings['privateKey']) ? str_pad('*', 11, '*') : NULL;
      $form['settings'][$environment]['privateKey'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Private key'),
        '#default_value' => $display,
        '#required' => ($environment == $this->environment),
      ];

      $form['settings'][$environment]['advanced_fraud_detection'] = [
        '#type' => 'radios',
        '#title' => $this->t('Send device data for advanced fraud detection'),
        '#options' => [
          -1 => $this->t('Force disable (regardless of Braintree control panel)'),
          0 => $this->t('Disabled'),
          1 => $this->t('Enabled'),
          2 => $this->t('Automatically enabled'),
        ],
        '#description' => $this->t('Enable this option before you enable advanced fraud detection in your Braintree account under the Processing Settings. It will be automatically enabled if you enable advanced fraud detection first in Braintree, but you risk a valid transaction being rejected.'),
        '#default_value' => $settings['advanced_fraud_detection'],
      ];
    }
    $form['buttons']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!class_exists('\Braintree\Base')) {
      $form_state->setErrorByName('settings', t('You must run composer to get the Braintree php library'));
    }
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_config = $this->config('braintree_donations.api_settings');
    $settings = $form_state->getValue('settings');
    foreach (['sandbox', 'production'] as $environment) {
      $current = (array) $this->config('braintree_donations.api_settings')->get($environment);
      if (strpos($settings[$environment]['privateKey'], '***') === 0) {
        $settings[$environment]['privateKey'] = $current['privateKey'];
      }
      elseif ($settings[$environment]['privateKey']) {
        $key_id = $settings[$environment]['encryption_key'];
        $key = $this->keyRepository->getKey($key_id)->getKeyValue();
        $settings[$environment]['privateKey'] = AES_128_CBC::encrypt($settings[$environment]['privateKey'], $key);
      }
      $api_config->set($environment, $settings[$environment]);
    }
    $api_config->save();
    drupal_set_message($this->t('The configuration options have been saved.'));
    // Clear the library cache since we may need to load a different JS file.
    $this->libraryDiscovery->clearCachedDefinitions();
  }

}
