<?php

namespace Drupal\braintree_donations\Utility;

use Drupal\Component\Utility\Crypt;

/**
 * Class \Drupal\braintree_donations\Utility\AES_128_CBC
 *
 * @see https://paragonie.com/blog/2015/05/if-you-re-typing-word-mcrypt-into-your-code-you-re-doing-it-wrong
 *
 * @see https://paragonie.com/blog/2015/05/using-encryption-and-authentication-correctly
 *
 * Base class that should work to handle any AES cipher in openssl with a
 * similar naming pattern for the cipher.
 */
class AES_128_CBC {

  /**
   * The openssl cipher for this class.
   */
  const CIPHER = 'AES-128-CBC';

  /**
   * Get the key size in bits
   *
   * @return int
   */
  public static function keySize() {
    $parts = explode('-', static::CIPHER);
    return (int) $parts[1];
  }

  /**
   * Validate a key and normalize to expected length.
   *
   * @param string $key
   * @return string
   *
   * @throws \Exception
   */
  protected static function normalizeKey($key) {
    $num_bytes = static::keySize() / 8;
    // Any valid CIPHER has at least a 128 bit key (16 bytes).
    if (strlen($key) < $num_bytes || $num_bytes < 16) {
      throw new \Exception("Needs at least a " . $num_bytes . "-byte key!");
    }
    // Use a simple, consistent KDF in case the input key is not binary.
    $key = substr(hash_hmac('sha512', static::CIPHER, $key, TRUE), 0, $num_bytes);
    return $key;
  }

  /**
   * Encrypt and MAC a message
   *
   * @param string $message
   *   Any message.
   * @param string $key
   *   A random binary string at least self::keySize() bits long.
   * @return string
   *   A base64 encoded MAC + encrypted data
   *
   * @throws \Exception
   */
  public static function encrypt($message, $key) {
    $key = self::normalizeKey($key);
    $iv_size = openssl_cipher_iv_length(static::CIPHER);
    $iv = Crypt::randomBytes($iv_size);

    $ciphertext = openssl_encrypt($message, static::CIPHER, $key, OPENSSL_RAW_DATA, $iv);

    $data = $iv . $ciphertext;
    // Construct a 128 bit HMAC and add to the front of the string.
    $final = substr(hash_hmac('sha512', $data, $key, TRUE), 0, 16) . $data;
    return base64_encode($final);
  }

  /**
   * @param $encrypted
   *   base64 encoded output from ::encrypt()
   * @param $key
   *   A random binary string at least self::KEY_SIZE bits long.
   * @return string
   *
   * @throws \Exception
   */
  public static function decrypt($encrypted, $key) {
    $message = base64_decode($encrypted);
    $key = self::normalizeKey($key);
    $iv_size = openssl_cipher_iv_length(static::CIPHER);
    $hmac = substr($message, 0, 16);
    $data = substr($message, 16);
    $expected_hmac = substr(hash_hmac('sha512', $data, $key, TRUE), 0, 16);
    if (Crypt::hashEquals($expected_hmac, $hmac)) {
      $iv = substr($data, 0, $iv_size);
      $ciphertext = substr($data, $iv_size);
      return openssl_decrypt($ciphertext, static::CIPHER, $key, OPENSSL_RAW_DATA, $iv);
    }
    throw new \Exception("HMAC mismatch.");
  }

}