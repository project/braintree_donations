
This module provides the scaffolding to create simple one time and
recurring donations through the Braintree Payments API. It is
assumed that most users will create a custom module and customize
the donation form such as to set a fix donation amount, add other
text or questions, etc.

As a simple example, in a custom module define a route
and a form buidler function:



mymodule.membership_join_now
  path: /membership/join-now
  defaults:
    _title: 'Join now'
    _form: \Drupal\mymodule\Form\MembershipForm
  requirements:
    _access: 'true'


class MembershipForm extends \Drupal\braintree_donations\Form\DropinForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mymodule_membership_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form  = parent::buildForm(array $form, $form_state);
    // Set a fixed donation amount.
    $form['braintree_donations']['description'] = array(
      '#markup' => '<p>Standard membership dues are $20 per year per person.</p>',
      '#weight' => -10,
    );
    $form['braintree_donations']['transaction']['amount']['#type'] = 'value';
    $form['braintree_donations']['transaction']['amount']['#value'] = 20.00;
    return $form;
  }
}


## Install and Configure

You must download and install composer to use this module. See:
http://getcomposer.org/download/

Run "composer install" to download and install the Braintree PHP library
and create an autoload.php file at vendor/autoload.php. This should be
done before enabling the module.

For a production (non-sandbox) environment you need to serve the JS
file from your own domain. For a sandbox environment it will be loaded
(if needed) from https://js.braintreegateway.com/v2/braintree.js

The js file will be placed at vendor/braintree-web/dist/braintree.js
by composer.

You can download it from https://js.braintreegateway.com/v2/braintree.js
or from https://github.com/braintree/braintree-web and place it at the
same location, especially if you need to test or use an updated version,
or simply delete the local one when using a sanbox environment to
pull in the remote one.

After enabling the module, you will need to configure the merchant ID
and keys at /admin/config/services/braintree-donations/api

By default a simple donation form is enabled and available at
/braintree-dropin-donations

In order to disble the default form or to configure customer
defaults including the default county code go to
/admin/config/services/braintree-donations

If you wish to use recurring payment you must set up one or more plans using
the Braintree control panel.  You should set each plan to have a trial period
that is the same as the billing cycle.  Set the amount to $1.

For example, a "monthly" plan should have a Billing Cycle of 1 month,
a Billing start of immediately after trial, and a Trial Period Duration
of 1 Month.

This scheme allows the initial form submit to send and process a transaction
to make the initial donation and store the credit card with an ID, as well
as to validate the card. If the following steps fail, at least the initial
dontion will have been collected.

## Test integration

Becuase correct integration with Braintree is among the key things to test,
you need to have installed the Braintree PHP and JS libraries and configured
a sandbox account in order to run the tests. This means the tests cannot run
on drupal.org currently.

In order to run the recurring donation test you need to have created at least
one plan in the sandbox account according to the instructions above.
