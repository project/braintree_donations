

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.BTDonations = {
    initialized: false,
    clientToken: ''
  };

  Drupal.BTDonations.setup = function($, scope) {
    if (typeof braintree != 'undefined' && Drupal.BTDonations.clientToken.length > 0 && Drupal.BTDonations.initialized == false) {
      Drupal.BTDonations.initialized = true;
      var options = {
        container: "edit-transaction-dropin"
      };
      if (drupalSettings.BraintreeDonations.advancedFraudDetect) {
        options.dataCollector = { kount: { environment: Drupal.BTDonations.environment } };
        options.onReady = function (braintreeInstance) {
          $('input[name="[transaction]deviceData"]').val(braintreeInstance.deviceData);
        }
      }
      braintree.setup(Drupal.BTDonations.clientToken, "dropin", options);
      // Show button when the document is ready.
      $(function() {
        $('div.braintree-donations-credit-card-submit').show();
      });
    }
  };

  var url = drupalSettings.path.baseUrl + 'braintree-donations/client-token';
  $.ajax({
    cache: false,
    type: 'POST',
    url: location.protocol + '//' + location.host + url,
    dataType: 'json',
    async: true,
    success: function (data) {
      if (!!data.token) {
        Drupal.BTDonations.clientToken = data.token;
        // If the braintree library loaded during the ajax call, initialize it.
        Drupal.BTDonations.setup($, 'header');
      }
    },
    error: function (xmlhttp) {
      //
    }
  });
  // Hide warning when the document is ready.
  $(function() {
    $('#braintree-no-js-warning').hide();
  });
})(jQuery, Drupal, drupalSettings);
