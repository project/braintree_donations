
(function ($) {
  // If the braintree library did not load during the ajax call, initialize it.
  Drupal.BTDonations.setup($, 'footer');
})(jQuery);
